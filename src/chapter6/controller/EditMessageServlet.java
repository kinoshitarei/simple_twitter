package chapter6.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.Message;
import chapter6.service.MessageService;

/**
 * Servlet implementation class EditMessage
 */
@WebServlet(urlPatterns = { "/edit" })
public class EditMessageServlet extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession session = request.getSession();
		List<String> errorMessages = new ArrayList<String>();
		String  msgId = request.getParameter("msgid");
		Message msg = null;

		if (!StringUtils.isEmpty(msgId) && msgId.matches("^[0-9]*$")) {
			msg = new MessageService().editselect(msgId);
		}

		if (msg == null) {
			errorMessages.add("不正なパラメータが入力されました");
			session.setAttribute("errorMessages", errorMessages);
			response.sendRedirect("./");
			return;
		}
		request.setAttribute("editMessage",msg);
		request.getRequestDispatcher("edit.jsp").forward(request, response);

	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		List<String> errorMessages = new ArrayList<String>();
		String msgId = request.getParameter("msgid");

		String formtext = request.getParameter("text");
		Message msg =  new Message();
		msg.setText(formtext);
		if (!isValid(msg.getText(), msgId, errorMessages)) {
			request.setAttribute("errorMessages", errorMessages);
			request.setAttribute("editMessage", msg);
			request.getRequestDispatcher("edit.jsp").forward(request, response);
			return;
		}

		msg.setId(Integer.parseInt(msgId));
		new MessageService().update(msg);
		response.sendRedirect("./");
	}


	private boolean isValid(String text, String msgId, List<String> errorMessages) {

		if (StringUtils.isBlank(text)) {
			errorMessages.add("入力してください");
		} else if (140 < text.length()) {
			errorMessages.add("140文字以下で入力してください");
		}
		if (errorMessages.size() != 0) {
			return false;
		}
		return true;
	}
}