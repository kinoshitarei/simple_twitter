package chapter6.dao;

import static chapter6.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import chapter6.beans.Message;
import chapter6.exception.SQLRuntimeException;

public class MessageDao {

	public void insert(Connection connection, Message message) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO messages ( ");
			sql.append("    user_id, ");
			sql.append("    text, ");
			sql.append("    created_date, ");
			sql.append("    updated_date ");
			sql.append(") VALUES ( ");
			sql.append("    ?, ");                  // user_id
			sql.append("    ?, ");                  // text
			sql.append("    CURRENT_TIMESTAMP, ");  // created_date
			sql.append("    CURRENT_TIMESTAMP ");   // updated_date
			sql.append(")");

			ps = connection.prepareStatement(sql.toString());

			ps.setInt(1, message.getUserId());
			ps.setString(2, message.getText());

			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public void delete(Connection connection, String msgId) {

		PreparedStatement ps = null;
		try {
			String sql = "DELETE FROM messages WHERE id = ?";

			ps = connection.prepareStatement(sql);

			ps.setInt(1, Integer.parseInt(msgId));

			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
	public Message editselect(Connection connection, String msgId) {

		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM messages WHERE id = ?";

			ps = connection.prepareStatement(sql);

			ps.setInt(1, Integer.parseInt(msgId));
			ResultSet rs = ps.executeQuery();
			List<Message> msg = toUserMsg(rs);

			if (msg.isEmpty()) {
				return null;
			} else {
				return msg.get(0);
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
	private List<Message> toUserMsg(ResultSet rs) throws SQLException {

		List<Message> usermsg = new ArrayList<Message>();
		try {
			while (rs.next()) {
				Message msg = new Message();
				msg.setId(rs.getInt("id"));
				msg.setText(rs.getString("text"));
				msg.setCreatedDate(rs.getTimestamp("created_date"));
				usermsg.add(msg);
			}
			return usermsg;
		} finally {
			close(rs);
		}
	}
	public void update(Connection connection, Message msg) {
		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();

			sql.append("UPDATE messages SET ");
			sql.append("    text = ?, ");
			sql.append("    updated_date = CURRENT_TIMESTAMP ");
			sql.append("WHERE id = ? ");

			ps = connection.prepareStatement(sql.toString());
			ps.setString(1, msg.getText());
			ps.setString(2, Integer.toString(msg.getId()));
			ps.executeUpdate();

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public void select(Connection connection, String msgId) {
		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT * FROM message WHERE id = ? ");
			ps = connection.prepareStatement(sql.toString());
			ps.setString(1, msgId);
			ps.executeUpdate();

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
}
